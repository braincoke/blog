---
title: "Wekan troubleshooting"
date: "2017-08-02 19:40:54 +0200"
tags: [Wekan, Troubleshooting]
header:
  teaser: assets/images/server/wekan-logo.png
excerpt: "Some solutions to encountered problems with Wekan"
---


## Error 503

Mongodb status ?

~~~bash
sudo systemctl status mongod
# Expected Output
● mongod.service - High-performance, schema-free document-oriented database
   Loaded: loaded (/lib/systemd/system/mongod.service; enabled; vendor preset: enabled)
   Active: active (running) since mer. 2017-08-02 12:12:35 CEST; 7h ago
     Docs: https://docs.mongodb.org/manual
 Main PID: 1029 (mongod)
    Tasks: 28
   Memory: 93.9M
      CPU: 3min 50.761s
   CGroup: /system.slice/mongod.service
           └─1029 /usr/bin/mongod --quiet --config /etc/mongod.conf
~~~

Reinstall pm2 (quickfix)
[Source](https://github.com/Unitech/pm2/issues/837)

* sudo npm uninstall -g pm2
* cd .npm/ && sudo rm -rf pm2*
* cd ~ && sudo rm -rf .pm2
* sudo npm install pm2 -g
* sudo chown nobody:nogroup ~/.pm2/*
