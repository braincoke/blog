---
title: "MongoDB backup"
excerpt: "
This post explores how you can backup your MongoDB database manually or
automatically using Studio 3T or `mongodump` for instance."
date: "2017-08-05 17:23:34 +0200"
tags: [Ubuntu 16.04, Backup, MongoDB, Studio3T]
type: Document
verified: 2017-08-05
header:
  image: assets/images/server/mongodb.png
  teaser: assets/images/server/mongodb-teaser.png
---

This post explores how you can backup your MongoDB database manually or
automatically using Studio 3T or `mongodump` for instance.

## Backup with Studio 3T

### Install and configure Studio 3T

Go to the [official website](https://studio3t.com/) and download Studio 3T.

Extract the software with `tar`:

~~~bash
tar xvzf studio-3t-linux-x64.tar.gz
~~~

Go into the newly created directory, and execute `bin/studio3T.sh`.
Then configure a new connection to your server. You can find more information
about how to do this in [this post about Robo3T]({% post_url 2017-08-04-use-robomongo-to-explore-mongodb %}).

### Backup your data

On the left panel, right click on the name of your connection, and choose `Export`.

1. A window opens asking you to choose the export format
2. Choose `mongodump`
3. The next step asks you to choose between folder and archive, I choose `archive`
4. The final step asks you where to write the archive.

Once you have done this you will have a mongodump that you can later import in
MongoDB.


## Mongo console commands

List dbs

~~~bash
show dbs
admin  0.078GB
local  0.078GB
~~~

Select a db

~~~bash
use admin
~~~

List collections

~~~bash
show collections
~~~

List users
~~~bash
show users
~~~

## Backup with mongodump

### From the server

~~~bash
mongodump --db admin --out pathToDirectory
~~~

### From another machine

~~~bash
ssh sammy.fr mongodump --db admin --out pathToDirectory
scp sammy.fr:pathToDirectory pathToBackupDirectory
~~~

This can be put in a script that will be executed by a crontab.
A user with very limited rights should be created to perform this action.
The user would have only write rights in a very specific folder, and only basic
read and execute rights to be able to perform the backups.

The private key should not be stored on the machine performing the backup.
Instead the use of a nano Yubikey, permanently inserted in the machine will prevent
malicious attackers from retrieving the private key.

## Automatic backup with crontab

~~~bash
mongo
~~~

Create an admin user if you don't have one
~~~mongo
use admin
db.createUser({
  user: "admin",
  pwd: "password",
  roles: [
    {role: "clusterAdmin", db: "admin"},
    {role: "readAnyDatabase", db: "admin"},
    "readWrite"
  ]},
  {w: "majority", wtimeout: 5000})
~~~

Create a user with read only rights to perform backups:
~~~mongo
db.createUser({
    user: "backupuser",
    pwd: "password",
    roles: ["read"]
})
~~~


Create a cron job to backup the database

~~~bash
# Mongodb admin backup
30 23 * * * DATE=$(date +'\%Y-\%m-\%d-\%H\%M'); mongodump --db admin -u backupuser -p password --out "/home/backupuser/backup/mongodb/admin/${DATE}_backup_mongodb"
~~~

Note that you need to escape the `%` character inside the cron editor when
formatting the date. You also need to reference the `DATE` variable with `${DATE}`
and not `$DATE` otherwise your command will not execute correctly !

~~~bash
# Mongodb admin cleanup
30 11 * * * BDIR=/home/backupuser/backup/mongodb/admin;
LAST=$(find $BDIR -mindepth 1 -maxdepth 1 -type d -printf '\%f\n' | sort -nr | head -1);
find $BDIR -mindepth 1 -maxdepth 1 ! -name "$LAST" -type d -exec rm -rf {} +
~~~
