---
title: "How to use Clonezilla"
date: "2017-12-11 23:33:00 +0100"
excerpt: "Backup and restore an entire disk with its partitions, or just one partition with Clonezilla."
tags: [Clonezilla, Backup, Disk]
---

## Start clonezilla

1. Plug your bootable USB with Clonezilla in your computer
2. Turn on your computer and access the boot menu
3. Choose your USB key to boot
4. Choose which Clonezilla you want to run, it is just a question of display
usually.
5. Choose your language (ex: `en_US.UTF-8 English`)
6. Choose your keymap
7. Choose `Start Clonezilla`


## Restoring a clone

Here we want to restore a clone of a full disk. The clone is stored as an image
on our USB drive.
The image we want to restore must be in a first level directory of our USB
drive.

{:.note .info}
If you made an image of a 200 GB disk, you will need a disk of at least 200 GB to restore your image. Even if you used 30 GB on your disk when you imaged it.

1. Start clonezilla
2. Choose device-image. Usually we clone the disk or partition to an image that
we store on a hard drive.
3. Choose `local_dev`. This means that we will use a USB drive to find the image.
4. Insert the USB device. Wait for 5 seconds and press ENTER.
5. Look for your USB drive in the list displayed. Mine is mounted in `/dev/sda`
6. Press CTRL-C
7. Choose `/dev/sda1` (this part depends on what you got in step 5)
8. Select the directory where you stored your image. You can only select first level directories.
9. Press Enter and choose the `Beginner` mode.
10. Choose `restore_disks`.
11. Choose the image you want to restore and continue with OK.
12. Choose the destination drive (on your PC) and continue with OK
13. Check if the image restorable, this can take several minutes
14. Read the messages displayed and ensure this is what you want to do !
15. Press Enter to continue.
