---
title: "HomeLab Introduction"
date: "2019-01-19 17:46:11 +0100"
excerpt: "Why and how I set up my homelab"
tags: [Ubuntu 18.04, HomeLab, self-hosted]
author_profile: false
sidebar:
  nav: "homelab"
verified: never
---


## Why a HomeLab

I like to find ways to automate and digitalize things around the house, be it the finance management, the torrent downloads or the development.

This is why I started to tinker with self-hosted applications like NextCloud, GitLab, Firefly III, JDownloader and so on.
As I was experimenting, I understood that I needed a specific infrastructure for these applications, which I call a homelab.

## HomeLab composition

There are mainy open-source self-hosted applications one can install in their homelab.
I mainly use Kickball's Github [list of self-hosted applications](https://github.com/Kickball/awesome-selfhosted) as a source of inspiration.

Some notable self-hosted applications are :
  
- **Firefly III** : a self-hosted money manager
- **GitLab** : a web interface for your Git projects
- **Radarr** : a interface to automate movie downloading
- **Sonarr** : same as Radarr but for TV.Shows
- **Lidarr** : same as Radarr but for music
- **Transmission** : used to download torrents
- **Organizr** : to have nice dashboard to quickly find all your self-hosted applications !


As of today, there is a project from Nick Busey called [HomelabOS](https://gitlab.com/NickBusey/HomelabOS) described as :
>Your very own offline-first open-source data-center!

This is an OS including a good number of self-hosted applications already pre-installed !

## HomeLab specifications

I decided to start my HomeLab on an intel NUC.
I also wanted my self-hosted apps to be easily maintained, updated or migrated if I needed.
This is why I chose to install every application with docker.

With docker, you can easily migrate your application to another server if you ever need it. You can have different applications using different versions of PHP or MySQL without it being a headache. And more importantly, you can have each application have their own IP address using *macvlan*.

## Docker macvlan

This last feature was very important to me. I like accessing my applications through URLs like `https://gitlab.home` rather than `https://nuc.home:38209`.
With `macvlan`, each docker container has their MAC address and IP address. You just have to set up an IP reservation in your DHCP to ensure the IPs are not taken.
Then you can access the container by their hostname !

## Docker-compose

Another greatness of docker is `docker-compose`.
With `docker-compose` you can create a `yaml` file describing a set of containers, their IP adresses, the volumes that should be mounted, the environment variables, and so on.

This is particularly useful when you are setting up an application that needs a database, a download client, etc...
Then your docker-compose file will contain the configuration of your application and the database or download client needed. When everything is written down in your yaml file, you will just need to run `docker-compose up` to fire up everything.


## Conclusion

In each `HomeLab` post I explain how I set up an application with docker an docker-compose. I keep things as straight and to the point as possible, but I might include some sceenshots to illustrate some configuration on the application itself.