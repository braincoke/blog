---
title: "Installing Jdownloader"
date: "2019-01-19 23:05:53 +0100"
excerpt: "Set up an automatic download client with docker"
tags: [HomeLab, self-hosted, JDownloader]
author_profile: false
sidebar:
  nav: "homelab"
verified: never
---

## JDownloader

JDownloader is a download manager, written in Java, which allows automatic download of groups of files from one-click hosting sites. JDownloader supports the use of premium accounts. Some parts, but not all, of JDownloader are open-source.

You can find its website [here](http://www.jdownloader.org)

## Setup

### Organizing files
First off we create a directory `Jdownloader2` on our system.

~~~bash
mkdir Jdownloader2
~~~

Inside this directory we create a file named `docker-compose.yml`
~~~bash
touch docker-compose.yml
~~~

### Organizing mounts
JDownloader will need a place to drop its download. By default JDownloader will place the downloads in `/root/Downloads` *inside the container*.
Which means that once the container is deleted, the downloads disappear with it.

Of course you should mount a directory to `/root/Downloads` so that when JDownloader downloads the files, they really go the mounted directory.

Here we will map our NAS download directory `~/NAS/Downloads`.

### Setting up the IP address

I chose to reserve the IP address `10.0.6.4` for JDownloader.
To do so, I went into my router DHCP dashboard and set a static DHCP address.
The rule maps the MAC address `02:42:0A:00:06:04` to the IP address `10.0.6.4` 
and also sets the hostname to `JDOWNLOADER`.

## Docker-compose

Here is the content of the docker-compose file

~~~yaml
 version: '2.2'
 services:
   web:
     container_name: 'jdownloader'
     image: 'jaymoulin/jdownloader:0.6.1-i386'
     hostname: 'jdownloader'
     restart: unless-stopped
     volumes:
       - './config:/opt/JDownloader/cfg'
       - '../NAS/Downloads:/root/Downloads'
     networks:
       host-macvlan-net:
         ipv4_address: 10.0.6.4
 networks:
   host-macvlan-net:
     external:
       name: host-macvlan-net

~~~

As you can see in the docker-compose file we have two volume mappings.

 - '../NAS/Downloads:/root/Downloads' maps the JDownloader download directory to our NAS download directory
 - './config:/opt/JDownloader/cfg' maps the JDownloader config directory to a directory on our system called `config`. 

## JDownloader configuration

Start by running the container with `docker-compose up -d`.
This will initiate JDownloader and create the configuration files.

Then you can execute the following command to configure your MyJDownloader 
login/password :
~~~bash
docker exec jdownloader configure email@email.com password
~~~

https://my.jdownloader.org is a service provided by `JDownloader` to let you 
access your JDownloader from anywhere.
It is also the only way to access it if you run it in headless mode.

Connect to https://my.jdownloader.org with your credentials to start using 
JDownloader. You can setup your configuration so that only local devices can 
connect to your JDownloader instance, but the authentication part will still 
be done by https://my.jdownloader.org.


## Backup script

~~~bash
#!/bin/zsh
#
#This script backs up the jdownloader2 config
#It must be executed by the root user to work properly
#
CONFIG_DIR=/home/braincoke/Jdownloader2/config
SHARED_BACKUP_DIR=/home/braincoke/backup/Jdownloader
NEW_BACKUP_NAME=$(echo -n "$(date +%s)_$(date -I | tr - _)")
NEW_BACKUP_DIR="$SHARED_BACKUP_DIR/$NEW_BACKUP_NAME"


# Create a new directory on the shared folder
echo "Creating new directory $NEW_BACKUP_DIR"
mkdir -p $NEW_BACKUP_DIR
# Backup the config dir
echo "Backing up JDownloader config..."
cp -r $CONFIG_DIR $NEW_BACKUP_DIR

# Create tar from backed up directories
echo "Creating tar file"
tar -cvzf "$NEW_BACKUP_DIR.tar" -C "$NEW_BACKUP_DIR" .

rm -r "$NEW_BACKUP_DIR"

echo "JDownloader backup finished"
~~~

