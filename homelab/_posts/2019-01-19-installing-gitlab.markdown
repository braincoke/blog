---
title: "Installing Gitlab"
date: "2019-01-19 23:40:25 +0100"
excerpt: "Set up a web interface to manage your Git projects"
tags: [HomeLab, self-hosted, GitLab]
author_profile: false
sidebar:
  nav: "homelab"
verified: never
---

## GitLab

GitLab is a web-based Git-repository manager with wiki and issue-tracking features, using an open-source license, developed by GitLab Inc.

You can find its website [here](http://www.gitlab.com)

## Setup

### Organizing files
First off we create a directory `Gitlab` on our system.

~~~bash
mkdir Gitlab
~~~

Inside this directory we create a file named `docker-compose.yml`
~~~bash
touch docker-compose.yml
~~~

### Organizing mounts

Gitlab will need three main folders :
- `config` : to store Gitlab's configuration
- `logs` : to store Gitlab's logs
- `data` : to store your git repositories

### Setting up the IP address

I chose to reserve the IP address `10.0.6.1` for Gitlab.
To do so, I went into my router DHCP dashboard and set a static DHCP address.
The rule maps the MAC address `02:42:0A:00:06:01` to the IP address `10.0.6.1` 
and also sets the hostname to `Gitlab`.

### Setting up the SMTP

I use Mailgun as a SMTP service. To configure it directly via the docker-compose
file, I set the environment variable `GITLAB_OMNIBUS_CONFIG`.

| Configuration                             | Value              |
| ----------------------------------------- | ------------------ |
| gitlab_rails['smtp_enable']               | true               |
| gitlab_rails['smtp_address']              | "smtp.mailgun.org" |
| gitlab_rails['smtp_port']                 | 587                |
| gitlab_rails['smtp_authentication']       | "plain"            |
| gitlab_rails['smtp_enable_starttls_auto'] | true               |
| gitlab_rails['smtp_user_name']            | "mailgun_username" |
| gitlab_rails['smtp_password']             | "mailgun_password" |
| gitlab_rails['smtp_domain']               | "gitlab.home"      |

## Docker-compose

Here is the content of the docker-compose file

~~~yaml
version: '2.2'
services:
  web:
    container_name: 'gitlab'
    image: 'gitlab/gitlab-ce:latest' 
    hostname: 'gitlab'
    restart: always
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://gitlab.home'
        # Add any other gitlab.rb configuration here, each on its own line
        gitlab_rails['smtp_enable'] = true
        gitlab_rails['smtp_address'] = "smtp.mailgun.org"
        gitlab_rails['smtp_port'] = 587
        gitlab_rails['smtp_authentication'] = "plain"
        gitlab_rails['smtp_enable_starttls_auto'] = true
        gitlab_rails['smtp_user_name'] = "mailgun_username"
        gitlab_rails['smtp_password'] = "mailgun_password"
        gitlab_rails['smtp_domain'] = "gitlab.home"
    ports:
      - '80:80'
      - '443:443'
      - '22:22'
    volumes:
      - './config:/etc/gitlab'
      - './logs:/var/log/gitlab'
      - './data:/var/opt/gitlab'
    networks:
      host-macvlan-net:
        ipv4_address: 10.0.6.1
networks:
  host-macvlan-net:
    external:
      name: host-macvlan-net
~~~

Start the container with `docker-compose up -d`

## Gitlab configuration

There is no special configuration required. Use it as you wish.


## Backup script

~~~bash
#!/bin/zsh
#
#This script backs up the Gitlab data and configuration to a shared folder
#It must be executed by the root user to work properly
#
CONFIG_DIR=/home/braincoke/Gitlab/config
LOCAL_BACKUP_DIR=/home/braincoke/Gitlab/data/backups
SHARED_BACKUP_DIR=/home/braincoke/backup/Gitlab
echo -n "Getting Gitlab version : "
GITLAB_VERSION=$(echo -n "$(docker exec -it gitlab gitlab-rake gitlab:env:info | grep -e "^Version:" | head -n 1 | cut -d$'\t' -f2)" | tr -d '\r')
echo "$GITLAB_VERSION"
NEW_BACKUP_NAME=$(echo -n "$(date +%s)_$(date -I | tr - _)_$GITLAB_VERSION")
NEW_BACKUP_DIR="$SHARED_BACKUP_DIR/$NEW_BACKUP_NAME"


# Create a new directory on the shared folder
echo "Creating new directory $NEW_BACKUP_DIR"
mkdir -p $NEW_BACKUP_DIR
# Backup the data
echo "Backing up Gitlab data..."
docker exec -t gitlab gitlab-rake gitlab:backup:create
find $LOCAL_BACKUP_DIR -type f -exec mv -t $NEW_BACKUP_DIR {} \+
# Backup the config
echo "Backing up Gitlab config..."
tar -czvf "$NEW_BACKUP_DIR/config.tar" $CONFIG_DIR
echo "Gitlab backup finished"

~~~