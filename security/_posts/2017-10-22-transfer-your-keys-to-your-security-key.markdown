---
title: "Transfer your GPG keys to your security key"
date: "2017-10-22 14:27:52 +0200"
tags: [Security key, Yubikey, Nitrokey, GPG, PGP]
author_profile: false
sidebar:
  nav: "security-practices"
---

## Configuring multiple keys

{:.note .warning}
You are discouraged to configure two security keys with the same subkeys.
Each security key should have a different set of subkeys (authentication, signing, encryption).

Each possessed security key should have a different subkeys. That way, if one security key gets stolen or lost, its subkeys can be revoked safely without losing access to the services it was tied to.

In addition to this, this is the best practice when using the security key in junction with gpg-agent to use the security key as a private ssh-key.
If you put the same subkeys on all your security keys then you won't be able to register and use your backup keys to perform SSH authentication since the card id is essential.
For instance if you try to use your backup security key to perform SSH authentication to Github.com, you will be asked for your main security key (the one you registered the SSH key with).

## Exporting to Yubikey smartCard

This article is extracted from [DrDuh's Yubikey Guide](https://github.com/drduh/YubiKey-Guide).


### Configure smartcard

Use GPG to configure YubiKey as a smartcard:

~~~bash
$ gpg2 --card-edit

Reader ...........: Yubico Yubikey 4 OTP U2F CCID
Application ID ...: D2760001240102010006055532110000
Version ..........: 2.1
Manufacturer .....: Yubico
Serial number ....: 05553211
Name of cardholder: [not set]
Language prefs ...: [not set]
Sex ..............: unspecified
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: not forced
Key attributes ...: rsa4096 rsa4096 rsa4096
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 3 3
Signature counter : 0
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]
~~~

### Change PINs

The default PIN codes are `12345678` for the Admin PIN (aka PUK) and `123456` for the PIN. The CCID-mode PINs can be up to 127 ASCII characters long.

The Admin PIN is required for some card operations, and to unblock a PIN that has been entered incorrectly more than three times. See the GnuPG documentation on [Managing PINs](https://www.gnupg.org/howtos/card-howto/en/ch03s02.html) for details.

~~~bash
gpg/card> admin
Admin commands are allowed

gpg/card> passwd
gpg: OpenPGP card no. D2760001240102010006055532110000 detected

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? 3
PIN changed.

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? 1
PIN changed.

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? q
~~~

### Set card information

Some fields are optional:

~~~bash
gpg/card> name
Cardholder's surname: Duh
Cardholder's given name: Dr

gpg/card> lang
Language preferences: en

gpg/card> login
Login data (account name): doc@duh.to

gpg/card> (Press Enter)

Application ID ...: D2760001240102010006055532110000
Version ..........: 2.1
Manufacturer .....: unknown
Serial number ....: 05553211
Name of cardholder: Dr Duh
Language prefs ...: en
Sex ..............: unspecified
URL of public key : [not set]
Login data .......: doc@duh.to
Private DO 4 .....: [not set]
Signature PIN ....: not forced
Key attributes ...: 2048R 2048R 2048R
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 3 3
Signature counter : 0
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]

gpg/card> quit
~~~


### Transfer keys

Transferring keys to YubiKey hardware is a one-way operation only, so make sure you've made a backup before proceeding. Previous gpg versions required the 'toggle' command before selecting keys. The currently selected key(s) are indicated with an `*`. When moving keys only one key should be selected at a time.

~~~bash
gpg2 --edit-key $KEYID

Secret key is available.

sec  rsa4096/0xFF3E7D88647EBCDB
    created: 2017-10-09  expires: never       usage: SC  
    trust: ultimate      validity: ultimate
ssb  rsa4096/0xBECFA3C1AE191D15
    created: 2017-10-09  expires: never       usage: S   
ssb  rsa4096/0x5912A795E90DD2CF
    created: 2017-10-09  expires: never       usage: E   
ssb  rsa4096/0x3F29127E79649A3D
    created: 2017-10-09  expires: never       usage: A   
[ultimate] (1). Dr Duh <doc@duh.to>
~~~

### Signature key

Select and move the signature key (you will be prompted for the key passphrase and admin PIN):

~~~bash
gpg> key 1

sec  rsa4096/0xFF3E7D88647EBCDB
    created: 2017-10-09  expires: never       usage: SC
    trust: ultimate      validity: ultimate
ssb* rsa4096/0xBECFA3C1AE191D15
    created: 2017-10-09  expires: never       usage: S
ssb  rsa4096/0x5912A795E90DD2CF
    created: 2017-10-09  expires: never       usage: E
ssb  rsa4096/0x3F29127E79649A3D
    created: 2017-10-09  expires: never       usage: A
[ultimate] (1). Dr Duh <doc@duh.to>

gpg> keytocard
Please select where to store the key:
   (1) Signature key
   (3) Authentication key
Your selection? 1

You need a passphrase to unlock the secret key for
user: "Dr Duh <doc@duh.to>"
4096-bit RSA key, ID 0xBECFA3C1AE191D15, created 2016-05-24
~~~

### Encryption key

Type `key 1` again to deselect and `key 2` to select the next key:

~~~bash
gpg> key 1
...
gpg> key 2

sec  rsa4096/0xFF3E7D88647EBCDB
    created: 2017-10-09  expires: never       usage: SC
    trust: ultimate      validity: ultimate
ssb  rsa4096/0xBECFA3C1AE191D15
    created: 2017-10-09  expires: never       usage: S
ssb* rsa4096/0x5912A795E90DD2CF
    created: 2017-10-09  expires: never       usage: E
ssb  rsa4096/0x3F29127E79649A3D
    created: 2017-10-09  expires: never       usage: A
[ultimate] (1). Dr Duh <doc@duh.to>
~~~

Move the encryption key to card:

~~~bash
gpg> keytocard
Please select where to store the key:
   (2) Encryption key
Your selection? 2
...
~~~
### Authentication key

Type `key 2` again to deselect and `key 3` to select the next key:

~~~bash
gpg> key 2
...
gpg> key 3

sec  rsa4096/0xFF3E7D88647EBCDB
    created: 2017-10-09  expires: never       usage: SC
    trust: ultimate      validity: ultimate
ssb  rsa4096/0xBECFA3C1AE191D15
    created: 2017-10-09  expires: never       usage: S
ssb  rsa4096/0x5912A795E90DD2CF
    created: 2017-10-09  expires: never       usage: E
ssb* rsa4096/0x3F29127E79649A3D
    created: 2017-10-09  expires: never       usage: A
[ultimate] (1). Dr Duh <doc@duh.to>
~~~

Move the authentication key to card:

~~~bash
gpg> keytocard
Please select where to store the key:
   (3) Authentication key
Your selection? 3
~~~

{% include info.html title="Configuring a backup" content="If you need to configure another yubikey with exactly the same subkeys, don't save but press CTRL+C. However you are discouraged to configure another Yubikey with exactly the same subkeys. Each security key should have their unique set of subkeys." %}

Save and quit:

~~~bash
gpg> save
~~~

### In case you really want two identical Yubikeys

In case you really want two identical Yubikeys, when you have finished configuring your first yubikey, don't enter `save` or the private subkeys will be deleted.

If you entered `save` you will have to :
  1. Move out all the contents of `$GNUPGHOME/private-keys-v1.d/` except for the master key.
  2. Restore the private keys from your previous backups (qrcode or paperkey).

Then you can configure your second Yubikey to have exactly the same subkeys.


Note that doing this is probably useless since:
 1. You won't be able to use the authentication key from your backup Yubikey (it is tied to the key id).
 2. The keys cannot be extracted from the Yubikey.

## Check your work

`ssb>` indicates a stub to the private key on smartcard:

~~~bash
gpg2 --list-secret-keys
/tmp.FLZC0xcM/pubring.kbx
-------------------------------------------------------------------------
sec   rsa4096/0xFF3E7D88647EBCDB 2017-10-09 [SC]
      Key fingerprint = 011C E16B D45B 27A5 5BA8  776D FF3E 7D88 647E BCDB
uid                            Dr Duh <doc@duh.to>
ssb>  rsa4096/0xBECFA3C1AE191D15 2017-10-09 [S]
ssb>  rsa4096/0x5912A795E90DD2CF 2017-10-09 [E]
ssb>  rsa4096/0x3F29127E79649A3D 2017-10-09 [A]
~~~
