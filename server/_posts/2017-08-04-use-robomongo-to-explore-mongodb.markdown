---
title: "Use Robomongo to explore MongoDB"
date: "2017-08-04 19:05:44 +0200"
excerpt: "Explore your MongoDB instance remotely with Robo3T"
tags: [Ubuntu 16.04, Robomongo, Robo3T, MongoDB]
header:
  teaser: assets/images/server/robo3t.png
verified: 2017-08-04
---

This post shows how you can use `Robo3T` formerly `Robomongo` to explore a
database on a remote server.

## Install Robo3T

Go to the [official website](https://robomongo.org/) and download the latest version of the software.

Extract the archive with

~~~bash
tar xvzf  robo3t-1.1.1-linux-x86_64-c93c6b0.tar.gz
~~~

Then navigate to the new directory and execute `bin/robo3t`.

## Configure Robo3T

To create a new connection, go in `File > Connect`.
You can then click on `create` to create a new connection.

### Connection tab

In the connection tab, choose:
* Type: `Direct Connection`
* Name: MyNewConnection
* Address: localhost
* Port: 27017

Note that we will connect to the server with a SSH private key, and then connect to the Mongo Database. That is why the address is `localhost`.

### SSH tab

* SSH Address: myserver.com
* SSH Port: 22
* SSH User Name: myusername
* SSH Auth Method: `Private Key`
* Private key: path/to/private/key/on/disk
* Passphrase: mypA$$phr@se55,;:+2

Click on `Test` to verify that everything works properly.

You can now explore, read and write to your database with Robo3T.
