---
title: "Make Apache and NodeJS live together"
excerpt: "In this post we explore how you can configure NodeJS and Apache to make them work
together with just one IP address."
resources:
  - name: 1and1 - Set up a NodeJS app for a website with Apache on Ubuntu 16.04
    link: https://www.1and1.com/cloud-community/learn/application/misc/set-up-a-nodejs-app-for-a-website-with-apache-on-ubuntu-1604/
tags: [Ubuntu 16.04, Apache, NodeJS, Reverse Proxy]
type: Document
---

In this post we explore how you can configure NodeJS and Apache to make them work
together with just one IP address.

## Resources

{% include resources_used.html %}

## Install Node.js

First off, update the server's packages and install `curl`:
~~~bash
sudo apt-get update
sudo apt-get install curl
~~~

Download the Node.js PPA:
~~~bash
curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
~~~

Then run the `nodesource_setup.sh` command to add the PPA to your server's package
cache:
~~~bash
sudo bash nodesource_setup.sh
~~~

{% include info.html content="*Note*: The script will update the server automatically." %}

Install Node.js:
~~~bash
sudo apt-get install nodejs
~~~
This automatically installs `npm`.

Finally, install the `build-essential` package for `npm`:
~~~bash
sudo apt-get install build-essential
~~~

## Create a Sample Node.js application

We create a separate directory in our website's document root for housing
Node.js applications:
~~~bash
sudo mkdir /var/www/sammy.fr/nodejs
~~~

In this directory we create the file `hello.js`:
~~~bash
sudo vim /var/www/sammy.fr/nodejs/hello.js
~~~

And we put the following content into the file:

{% highlight2 js caption=/var/www/sammy.fr/nodejs/hello.js %}
#!/usr/bin/env nodejs
var http = require('http');
http.createServer(function (request, response) {
   response.writeHead(200, {'Content-Type': 'text/plain'});
   response.end('Hello World! Node.js is working correctly.\n');
}).listen(8080);
console.log('Server running at http://127.0.0.1:8080/');
{% endhighlight2 %}

We then make the file executable:
~~~bash
sudo chmod 755 /var/www/sammy.fr/nodejs/hello.js
~~~

## Install PM2
We use `npm` to make the file executable:

~~~bash
sudo npm install -g pm2
~~~

We start the script with the following command:
~~~bash
sudo pm2 start hello.js
~~~

__As root__ we add PM2 to the startup scripts, so that it will automatically
restart if the server is rebooted:
~~~bash
sudo pm2 startup systemd
~~~

## Configure Apache
To access the Node.js script from the web, install the Apache modules `proxy` and `proxy_http` with the commands:

~~~bash
sudo a2enmod proxy
sudo a2enmod proxy_http
~~~

Once the installation is complete, restart Apache for the changes to take effect:

~~~bash
sudo service apache2 restart
~~~

Next, you will need to add the Apache proxy configurations.
These directives need to be inserted into the `VirtualHost` command block in the
site's main Apache configuration file.

By common convention, this Apache configuration file is usually `/etc/apache2/sites-available/sammy.fr.conf` on Ubuntu.

{% include info.html content="__Note__: The location and filename of a site's Apache configuration file can vary." %}

Edit this file with your editor of choice, for example with the command:

~~~bash
sudo vim /etc/apache2/sites-available/example.com.conf
~~~

Scroll through the file until you find the `VirtualHost` command block,
which will look like:

{% highlight2 apache caption=/etc/apache2/sites-available/example.com.conf %}
<VirtualHost *:80>
ServerName sammy.fr
    <Directory "/var/www/sammy.fr/public_html">
    AllowOverride All
    </Directory>
</VirtualHost>
{% endhighlight2 %}


Add the following to VirtualHost command block:
{% highlight2 apache caption=/etc/apache2/sites-available/example.com.conf %}

  ProxyRequests Off
  ProxyPreserveHost On
  ProxyVia Full
  <Proxy *>
     Require all granted
  </Proxy>

  <Location /nodejs>
     ProxyPass http://127.0.0.1:8080
     ProxyPassReverse http://127.0.0.1:8080
  </Location>
{% endhighlight2 %}

Be sure to put these lines outside any Directory command blocks. For example:

~~~apache
<VirtualHost *:80>
ServerName sammy.fr

   ProxyRequests Off
   ProxyPreserveHost On
   ProxyVia Full
   <Proxy *>
      Require all granted
   </Proxy>

   <Location /nodejs>
      ProxyPass http://127.0.0.1:8080
      ProxyPassReverse http://127.0.0.1:8080
   </Location>

    <Directory "/var/www/sammy.fr/public_html">
    AllowOverride All
    </Directory>
</VirtualHost>
~~~

Save and exit the file, then restart Apache for the changes to take effect:

~~~bash
sudo service apache2 restart
~~~

After Apache restarts, you can test the application by viewing it in a browser.
You should see the message, "Hello World! Node.js is working correctly."
