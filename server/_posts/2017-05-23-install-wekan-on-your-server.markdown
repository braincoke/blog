---
title: "Install Wekan on your server"
excerpt: "Wekan is a Trello like board that lets you organize your projects using the
Kanban principles."
resources:
  - name: NodeJS and Apache
    link: https://www.1and1.com/cloud-community/learn/application/misc/set-up-a-nodejs-app-for-a-website-with-apache-on-ubuntu-1604/
  - name: Wekan github
    link: https://github.com/wekan/wekan
tags: [Ubuntu 16.04, Apache, NodeJS, Wekan, self-hosted, Kanban]
author_profile: false
sidebar:
  nav: "self-hosted"
verified: 2017-05-23
---

Wekan is a Trello like board that lets you organize your projects using the
Kanban principles.
In this post we will assume that you own the domain `sammy.fr` and that
you want to host your Wekan board under the subdomain `wekan.sammy.fr` using
Apache.

## Resources

{% include resources_used.html %}

## Configure Apache and Let's Encrypt

First we need to create an apache configuration file. We copy an existing one
that will serve as a template:

~~~bash
cd /etc/apache2/sites-available
sudo cp sammy.fr.conf wekan.sammy.fr.conf
~~~

We modify the file with `sudo vim wekan.sammy.fr.conf` and set the following
variables:
* DocumentRoot: /var/www/wekan.sammy.fr/
* ServerName: wekan.sammy.fr
* ServerAlias: www.wekan.sammy.fr


We also add the following to VirtualHost command block, note the trailing slash
in the `ProxyPass` lines:

{% highlight2 apache caption=/etc/apache2/sites-available/wekan.sammy.fr.conf %}
   ProxyRequests Off
   ProxyPreserveHost On
   ProxyVia Full
   <Proxy *>
      Require all granted
   </Proxy>

   ProxyPass "/" http://127.0.0.1:8080/
   ProxyPassReverse "/" http://127.0.0.1:8080/   
{% endhighlight2 %}


Since Wekan uses NodeJS, we need to use a reverse proxy so that Apache can redirect
the queries to NodeJS.

Finally we activate Let's Encrypt (choose Expand)

{% include note.html content="Get the command with all your websites with this `find  /etc/apache2/sites-available -type f -name '*.fr.conf'  | rev | cut -d '/' -f1 | cut -d '.' -f2- | rev | xargs -I {} echo \"-d {} -d www.{}\" | tr '\n' ' ' | xargs -I {} echo \"sudo certbot --apache {}\"` "%}

We enable the site with

~~~bash
sudo a2ensite wekan.sammy.fr.conf
sudo service apache2 reload
~~~

## Install MongoDB

Wekan uses MongoDB to store its information.

{% include note.html content="MongoDB 3.4.1 is [not compatible](https://github.com/wefork/wekan/issues/58) with Wekan
at the moment. We need to install 3.2.x"%}

We add the MongoDB repository and install MongoDB and its tools:
~~~bash
# MongoDB for Ubuntu 16.04
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt update
sudo apt install -y mongodb-org mongodb-org-server mongodb-org-shell mongodb-org-mongos mongodb-org-tools
~~~

We then activate MongoDB
~~~bash
sudo systemctl start mongod
sudo systemctl enable mongod
~~~


## Install Meteor

As you might have noticed already, Wekan is built using the Meteor web framework, so we need to install this as well. This can be done easily using their install script (read it if you don't trust it).

~~~bash
curl https://install.meteor.com/ | sh
~~~

## Install and Build Wekan

Before installing Wekan, we need to install NodeJS. Wekan uses a very specific
version of NodeJS so we need to carefully follow the installation procedure.


### Install nodejs

We start by cleaning old modules
~~~bash
sudo rm -rf /usr/local/lib/node_modules
sudo rm -rf ~/.npm
~~~

We download Wekan in `~/Git/`. We will install it later.
~~~bash
# Download Wekan
git clone https://github.com/wekan/wekan
cd wekan
git submodule init
git submodule update
#### OPTIONAL: test pull request
## git checkout -b dwrensha-profile-bugfix devel
## git pull https://github.com/dwrensha/wekan.git profile-bugfix
~~~

We install Node.js 4.8.1
~~~bash
sudo apt install build-essential g++ capnproto nodejs nodejs-legacy npm git curl
sudo npm -g install n
## if the previous fails (and asks you to run as sudo) just rm /home/<username>/node_modules and retry
sudo n 4.8.1
sudo npm -g install npm@latest
sudo npm -g install node-gyp
sudo npm -g install node-pre-gyp
sudo npm -g install fibers
~~~

And we finish by installing Wekan npm dependencies
~~~bash
sudo chown -R $USER:$(id -gn $USER) /home/sammy/.config
npm install
~~~

### Build

In `~/Git/wekan`, we build the project:
~~~bash
rm -rf .build
meteor build .build --directory
cp fix-download-unicode/cfs_access-point.txt .build/bundle/programs/server/packages/cfs_access-point.js
sed -i "s|build\/Release\/bson|browser_build\/bson|g" .build/bundle/programs/server/npm/node_modules/meteor/cfs_gridfs/node_modules/mongodb/node_modules/bson/ext/index.js
cd .build/bundle/programs/server/npm/node_modules/meteor/npm-bcrypt
rm -rf node_modules/bcrypt
npm install bcrypt
cd .build/bundle/programs/server
npm install
~~~

### Install in /var/www/

As stated earlier, we install Wekan in `/var/www/wekan.sammy.fr`:
~~~bash
sudo mkdir /var/www/wekan.sammy.fr/
sudo cp -r ~/Git/wekan/.build/bundle/* /var/www/wekan.sammy.fr/
sudo chown -R $USER:$USER /var/www/wekan.sammy.fr/
~~~

## Start Wekan

Wekan uses NodeJS. However since our IP address is already tied to our Apache
server, we need to have a reverse proxy so that when a user goes to wekan.sammy.fr, Apache redirects the user to NodeJS.

To do this we install `pm2` with `npm`
~~~bash
## Install PM2
sudo npm install -g pm2
~~~

Then we create a script `~/scripts/run-wekan.sh` and we place the following content
in it:
~~~vim
cd /var/www/wekan.sammy.fr
export MONGO_URL='mongodb://127.0.0.1:27017/admin'
export ROOT_URL='https://wekan.sammy.fr:8080'
export MAIL_URL='smtp://myusername:mypass@smtpserver:465/'
# This is local port where Wekan Node.js runs.
export PORT=8080
pm2 start main.js
pm2 startup systemd
~~~

Note that since we use Let's Encrypt and redirect all http traffic to https, we
must use https in the `ROOT_URL`.

We make the script executable and run it to start Wekan:
~~~bash
chmod +x ~/scripts/run-wekan.sh
~/scripts/run-wekan.sh
~~~

{% include note.html content="**Note**: the ̀line `pm2 startup systemd` makes
Wekan start automatically after a reboot. So you only have to run the script
once."%}

The variable `MAIL_URL` contains the smtp configuration. I personally own a
SMTP server at OVH and configured it like this `smtp://mail@sammy.fr:mypass@ssl0.ovh.net:465/`.

The smtp server is very important. It will allow users to share boards and
recover their passwords.

## Configure Wekan

Now you can connect to Wekan, create your first user and first boards.

It is advised to disable the signup in the administation panel. You can always
invite people to Wekan if they want to create a user.



Note that you can change your smtp configuration in
`admin panel > Settings > Email`:

Here is my current configuration:
~~~vim
SMTP Host : ssl0.ovh.net
SMTP Port : 465
Username : mail@sammy.fr
Password : myPassword
TLS support : enabled
From : sammy <mail@sammy.fr>
~~~
