---
title: "Installing entware on tomato advanced"
excerpt: "entware is a tool suite allowing you to use tools such as `tcpdump` on your router. The following is a tutorial extracted from [Tomato USB](http://tomatousb.org/tut:optware-installation) explaining how to install entware."
source:
    name: Tomato USB
    link: http://tomatousb.org/tut:optware-installation
tags: [Entware, Tomato Shibby, Advanced Tomato, Router, Netgear R7000]
type: Document
---

entware is a tool suite allowing you to use tools such as `tcpdump` on your router. The following is a tutorial extracted from [Tomato USB](http://tomatousb.org/tut:optware-installation) explaining how to install entware.

## Disk formatting

It is recommended that you format your USB storage device to `ext3` (for HDDs) or `etx2` (for flash drives, to minimize writes to the flash and avoid wearing it too much). If you are unfamiliar with Linux disk formatting tools (`fdisk` and `mkfs`), it is suggested that you format the disk using a graphical tool from a Linux desktop (such as Ubuntu).

The suggested size for the partition is max 1 Gbyte, or even less (512Mbyte is already more than enough). While formatting, use the label "optware" for the partition that you intend to use for Optware.

If you want to store additional data in the USB key, you can simply use a different partition (or store it within the Optware's partition, if you want to).

## Router configuration

  * Log into your router with the web interface. Usually `http://192.168.1.1`.
  * Go to `USB` and `NAS » USB Support`
  * Enable "Core USB Support", "USB 2.0 Support", "USB Storage Support", All "File Systems Support", and also enable "Automount"
  * Go to `Administration » Scripts » Init` and add the following line

{% highlight2 bash caption=Init %}
echo "LABEL=optware /opt ext3 defaults 1 1" >> /etc/fstab
{% endhighlight2 %}

  * Then click [SAVE].


This tells Tomato to automatically mount the partition named "optware" in `/opt`. You can change the filesystem from "ext3" to "ext2" in case you formatted it with ext2.

## Checking configuration

  * Reboot your router.
  * Plug the USB disk/pendrive if you have not already.
  * In the web interface, go to `USB` and `NAS » USB Support » Attached Devices` and check the list. There should be your USB drive, with a text like Partition "optware" ext3 is mouned on `/opt`.
  * You can mount/umount the partition from the web interface.

Download the entware shell:
~~~bash
cd /opt
wget http://qnapware.zyxmon.org/binaries-armv7/installer/entware_install_arm.sh
# Output
Connecting to qnapware.zyxmon.org (81.4.123.217:80)
entware_install_arm. 100% |*********************************************************************************************************************************|  1711   0:00:00 ETA
~~~

Make it executable:
~~~bash
chmod +x entware_install_arm.sh
~~~

Execute it to install entware.
~~~bash
./entware_install_arm.sh
~~~

{% highlight2 bash caption=Output collapsible=true expanded=false%}
Info: Checking for prerequisites and creating folders...
Warning: Folder /opt exists!
Info: Opkg package manager deployment...
Connecting to qnapware.zyxmon.org (81.4.123.217:80)
opkg                 100% |*********************************************************************************************************************************|   128k  0:00:00 ETA
Connecting to qnapware.zyxmon.org (81.4.123.217:80)
opkg.conf            100% |*********************************************************************************************************************************|   146   0:00:00 ETA
Connecting to qnapware.zyxmon.org (81.4.123.217:80)
ld-2.20.so           100% |*********************************************************************************************************************************|   131k  0:00:00 ETA
Connecting to qnapware.zyxmon.org (81.4.123.217:80)
libc-2.20.so         100% |*********************************************************************************************************************************|  1190k  0:00:00 ETA
Info: Basic packages installation...
Downloading http://qnapware.zyxmon.org/binaries-armv7/Packages.gz.
Updated list of available packages in /opt/var/opkg-lists/packages.
Installing glibc-opt (2.20-5) to root...
Downloading http://qnapware.zyxmon.org/binaries-armv7/glibc-opt_2.20-5_armv7soft.ipk.
Installing libc (2.20-8b) to root...
Downloading http://qnapware.zyxmon.org/binaries-armv7/libc_2.20-8b_armv7soft.ipk.
Installing libgcc (4.8.3-8b) to root...
Downloading http://qnapware.zyxmon.org/binaries-armv7/libgcc_4.8.3-8b_armv7soft.ipk.
Installing libstdcpp (4.8.3-8b) to root...
Downloading http://qnapware.zyxmon.org/binaries-armv7/libstdcpp_4.8.3-8b_armv7soft.ipk.
Installing libpthread (2.20-8b) to root...
Downloading http://qnapware.zyxmon.org/binaries-armv7/libpthread_2.20-8b_armv7soft.ipk.
Installing librt (2.20-8b) to root...
Downloading http://qnapware.zyxmon.org/binaries-armv7/librt_2.20-8b_armv7soft.ipk.
Installing locales (2.20-8b) to root...
Downloading http://qnapware.zyxmon.org/binaries-armv7/locales_2.20-8b_armv7soft.ipk.
Installing findutils (4.5.14-1) to root...
Downloading http://qnapware.zyxmon.org/binaries-armv7/findutils_4.5.14-1_armv7soft.ipk.
Installing terminfo (5.9-2) to root...
Downloading http://qnapware.zyxmon.org/binaries-armv7/terminfo_5.9-2_armv7soft.ipk.
Configuring libgcc.
Configuring libc.
Configuring terminfo.
Configuring locales.
Entware-arm uses separate locale-archive file independent from main system
Creating locale archive - /opt/usr/lib/locale/locale-archive
Adding en_EN.UTF-8
Adding ru_RU.UTF-8
/opt/usr/lib/locale/locale-archive found
You can download locale sources from http://qnapware.zyxmon.org/sources/i18n.tar.gz
You can add new locales for Entware-arm using /opt/bin/localedef.new
Configuring libpthread.
Configuring libstdcpp.
Configuring librt.
Configuring findutils.
Configuring glibc-opt.
Info: Congratulations!
Info: If there are no errors above then Entware.arm successfully initialized.
Info: Add /opt/bin & /opt/sbin to your PATH variable
Info: Add '/opt/etc/init.d/rc.unslung start' to startup script for Entware.arm services to start
Info: Found a Bug? Please report at https://github.com/zyxmon/entware-arm/issues
{% endhighlight2 %}
