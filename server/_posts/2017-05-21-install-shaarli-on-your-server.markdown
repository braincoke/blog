---
title: "Install Shaarli on your server"
excerpt: "Shaarli is a self-hosted, open-source bookmark manager. This post explains how
to install Shaarli on your server."
resources:
  - name: Server requirements
    link: https://github.com/shaarli/Shaarli/wiki/Server-requirements
  - name: Material theme
    link: https://github.com/kalvn/Shaarli-Material
tags: [Ubuntu 16.04, Shaarli, self-hosted, Bookmarks]
author_profile: false
sidebar:
  nav: "self-hosted"
verified: 2017-05-19
---

Shaarli is a self-hosted, open-source bookmark manager. This post explains how
to install Shaarli on your server.

## Resources

{% include resources_used.html %}

## Prerequisites

Having followed the set **Setting up your own server**:
TODO

{% include info.html content="**Note**: When creating the virtual host configuration use the data given below." %}

When you create the file `/etc/apache2/sites-available/shaarli.sammy.fr.conf`
use the following configuration:
{% highlight2 apache caption=/etc/apache2/sites-available/shaarli.sammy.fr.conf %}
<VirtualHost *:80>
    ServerName   shaarli.sammy.fr
    ServerAlias www.shaarli.sammy.fr
    DocumentRoot /var/www/shaarli.sammy.fr/

    <Directory /var/www/shaarli.sammy.fr/>
            AllowOverride All
            Options Indexes FollowSymLinks MultiViews
            Order allow,deny
            allow from all
    </Directory>

    LogLevel  warn
    ErrorLog  /var/log/apache2/shaarli-error.log
    CustomLog /var/log/apache2/shaarli-access.log combined
</VirtualHost>
{% endhighlight2 %}


Then you can issue a ssl certificate with Let's Encrypt. Note that you can choose
'Expand' if you want this site to use the previously generated SSL certificate.
When running certbot to generate the certificate you must provide **ALL** the
websites concerned by the SSL certificate (you have to give the website you
might have already gave).

And don't forget to enable the site with
~~~bash
sudo a2ensite /etc/apache2/sites-available/shaarli.sammy.fr
sudo service apache2 reload
~~~


## Server requirements

#### Composer

Shaarli uses Composer to resolve and install third-party PHP
dependencies. You can see how to install composer [here]({% post_url 2017-05-18-install-wallabag-on-your-server %}#installing-composer)

#### Extensions

* openssl
* php-gd : thumbnail resizing
* php-intl : localized text sorting (e.g.  `e->è->f`)
* php-curl : using cURL for fetching webpages and thumbnails in a more robust way

~~~bash
sudo apt-get install php-gd php-intl php-curl
~~~

## Install Shaarli

We clone the project in /var/www/shaarli.sammy.fr
~~~bash
mkdir /var/www/shaarli.sammy.fr/
chown -R $USER:$USER /var/www/shaarli.sammy.fr/
cd /var/www/
git clone -b v0.8 https://github.com/shaarli/Shaarli.git . shaarli.sammy.fr
~~~

And then install/update third-party dependencies
~~~bash
cd shaarli.sammy.fr
composer install --no-dev
~~~

~~~bash
chown -R www-data:www-data /var/www/shaarli.sammy.fr/
~~~

## Instal the material theme

The default theme is quite ugly. You can find a material theme [here](https://github.com/kalvn/Shaarli-Material).

To install the material theme, you will have to download and extract the directory
`material` into `/var/www/shaarli.sammy.fr/tpl`.

First, prepare the `material` directory:
~~~bash
cd /var/www/shaarli.sammy.fr/tpl
mkdir material
cd material
~~~

Then download and extract the theme
~~~bash
wget https://github.com/kalvn/Shaarli-Material/archive/v0.8.3.tar.gz
sudo tar -xvf v0.8.3.tar.gz Shaarli-Material-0.8.3/material/
mv Shaarli-Material-0.8.3/material/* .
rm -r Shaarli-Material-0.8.3
~~~

To enable the theme:
* Shaarli < v0.9.0: edit data/config.json.php and set the value of raintpl_tpl to the new theme name: "raintpl_tpl": "tpl\/my-template\/"
* Shaarli >= v0.9.0: select the theme through the Tools page

In our case we edit `/var/www/shaarli.sammy.fr/data/config.json.php` and
modify it to have the line:

{% highlight2 vim caption=/var/www/shaarli.sammy.fr/data/config.json.php %}
"raintpl_tpl": "tpl\/material\/"
{% endhighlight2 %}
