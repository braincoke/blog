---
title: "Install Wallabag on your server"
excerpt: "Wallabag is an open source alternative to *Pocket*. With Wallabag you can save articles found on the web to read them later."
tags: [Ubuntu 16.04, Wallabag, self-hosted]
author_profile: false
sidebar:
  nav: "self-hosted"
verified: 2017-05-19
---

Wallabag is an open source alternative to *Pocket*. With Wallabag you can save articles found on the web to read them later.

## Prerequisites

* Add a non root user on your server - [link]({{ site.base_url }}{% post_url 2017-05-07-initial-server-setup-with-ubuntu-16-04 %})
* Install a LAMP stack - [link]({{ site.base_url}}{% post_url 2017-05-10-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-1604 %})
* Configure your Apache virtual hosts - [link]({{ site.base_url }}{% post_url 2017-05-10-set-up-apache-virtual-hosts-on-ubuntu-1604 %})
* Install Let's Encrypt - [link]({{ site.base_url }}{% post_url 2017-05-10-secure-apache-with-lets-encrypt-on-ubuntu-1604 %})

## Sources

A part of this work is based on the work of [Xiao Guoan](https://disqus.com/by/xiao_guoan/) that you can find on [Linuxbabe](https://www.linuxbabe.com/ubuntu/install-wallabag-ubuntu-16-04). However you can also find the same documentation on the official [Wallabag documentation](https://doc.wallabag.org/fr/admin/installation/installation.html).

## Installing composer

### Getting composer.phar

As stated on the [official Wallabag installation procedure](https://doc.wallabag.org/en/admin/installation/requirements.html)
compose is used to install the large number of PHP libraries used to function.

The following procedure is extracted from the [composer website](https://getcomposer.org/doc/00-intro.md). At the end of the procedure
we get a `composer.phar` file.

~~~bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
~~~

{% include info.html content="You should always check the [official documentation](https://getcomposer.org/download/) to ensure that the method has not changed" %}

The file `composer.phar` should be accessible by the wallabag installation script.
To do so we can either place the file in the correct place, or simply install it
globally.

### Installing globally

To install globally we can modify the `PATH` variable to add the folder containing
the file, or move the file to a folder listed in the `PATH` variable.
We choose the latter:

~~~bash
sudo mv composer.phar /usr/local/bin/composer
~~~

## Installing php extensions

The following php extensions are required for wallabag:
-php-session
-php-ctype
-php-dom
-php-hash
-php-simplexml
-php-json
-php-gd
-php-mbstring
-php-xml
-php-tidy
-php-iconv
-php-curl
-php-gettext
-php-tokenizer
-php-bcmath

Some of them might already be installed with PHP.
Before installing them, let's take a look at some useful commands that will help
us work with php extensions.

### Some general information
See if php-session module is installed :
~~~bash
php -m | grep session
# Output
session
~~~

Search for a package :
~~~bash
apt-cache search php- | grep packagename
~~~

Install a package :
~~~bash
sudo apt-get install packagename
~~~

### Install missing extensions

Here are the php extensions usually missing
~~~bash
sudo apt-get install php-bcmath php-curl php-tidy
~~~

## Create a database and user for Wallabag

The procedure used to create the database and a database user is not detailed
on the official website. The following procedure was found on [this website](
  https://www.linuxbabe.com/ubuntu/install-wallabag-ubuntu-16-04
  )

From a terminal, login to mysql:
~~~bash
mysql -u root -p
~~~

The mysql console starts. Create a database and name it 'wallabag':
~~~sql
create database wallabag;
~~~

Create a new user in the database, we will use this user's credentials to let
wallabag use the database:
~~~sql
create user 'sammy'@'localhost' IDENTIFIED BY 'my_password*84';
~~~

We give privileges on the wallabag database to our new user
~~~sql
grant all privileges on wallabag.* to sammy@localhost identified by 'my_password*84';
~~~

We flush the privileges so that they are refreshed:
~~~sql
flush privileges;
~~~

And we are done with the mysql console:
~~~sql
exit;
~~~

## Install Wallabag

First we need to install some programs that might not be installed on our server
already :
~~~bash
sudo apt-get install make unzip zip
~~~

Then we clone the wallabag project in a newly created folder named 'Git':

~~~bash
mkdir Git
cd Git
git clone https://github.com/wallabag/wallabag.git
~~~

We should have a new directory created named 'wallabag'. Let's proceed to the
installation :

~~~bash
cd wallabag/
make install
~~~

Here is the expected output (click to collapse):

{% highlight2 bash caption=Output collapsible=true expanded=false %}
composer.phar not found, we'll see if composer is installed globally.
HEAD is now at 82307d8... Release wallabag 2.2.3
Loading composer repositories with package information
Installing dependencies from lock file
Package operations: 106 installs, 0 updates, 0 removals
  - Installing guzzlehttp/ringphp (1.1.0): Downloading (100%)         
  - Installing guzzlehttp/guzzle (5.3.1): Downloading (100%)         
  - Installing bdunogier/guzzle-site-authenticator (v1.0.0-beta1): Downloading (100%)         
  - Installing behat/transliterator (v1.1.0): Downloading (100%)         
  - Installing twig/twig (v1.33.2): Downloading (100%)         
  - Installing symfony/polyfill-util (v1.3.0): Downloading (100%)         
  - Installing paragonie/random_compat (v1.4.2): Downloading (100%)         
  - Installing symfony/polyfill-php70 (v1.3.0): Downloading (100%)         
  - Installing symfony/polyfill-php56 (v1.3.0): Downloading (100%)         
  - Installing symfony/polyfill-mbstring (v1.3.0): Downloading (100%)         
  - Installing symfony/symfony (v3.2.8): Downloading (100%)         
  - Installing symfony/polyfill-intl-icu (v1.3.0): Downloading (100%)         
  - Installing psr/log (1.0.2): Downloading (100%)         
  - Installing psr/cache (1.0.1): Downloading (100%)         
  - Installing doctrine/lexer (v1.0.1): Downloading (100%)         
  - Installing doctrine/inflector (v1.1.0): Downloading (100%)         
  - Installing doctrine/collections (v1.3.0): Downloading (100%)         
  - Installing doctrine/cache (v1.6.1): Downloading (100%)         
  - Installing doctrine/annotations (v1.2.7): Downloading (100%)         
  - Installing doctrine/common (v2.6.2): Downloading (100%)         
  - Installing craue/config-bundle (1.4.2): Downloading (100%)         
  - Installing doctrine/doctrine-cache-bundle (1.3.0): Downloading (100%)         
  - Installing zendframework/zend-eventmanager (3.0.1): Downloading (100%)         
  - Installing zendframework/zend-code (2.6.3): Downloading (100%)         
  - Installing ocramius/proxy-manager (1.0.2): Downloading (100%)         
  - Installing doctrine/dbal (v2.5.12): Downloading (100%)         
  - Installing doctrine/migrations (v1.5.0): Downloading (100%)         
  - Installing jdorn/sql-formatter (v1.2.17): Downloading (100%)         
  - Installing doctrine/doctrine-bundle (1.6.7): Downloading (100%)         
  - Installing doctrine/doctrine-migrations-bundle (v1.2.1): Downloading (100%)         
  - Installing willdurand/jsonp-callback-validator (v1.1.0): Downloading (100%)         
  - Installing friendsofsymfony/jsrouting-bundle (1.6.0): Downloading (100%)         
  - Installing friendsofsymfony/oauth2-php (1.2.1): Downloading (100%)         
  - Installing friendsofsymfony/oauth-server-bundle (1.5.2): Downloading (100%)         
  - Installing willdurand/negotiation (v2.3.1): Downloading (100%)         
  - Installing friendsofsymfony/rest-bundle (2.2.0): Downloading (100%)         
  - Installing friendsofsymfony/user-bundle (dev-master e0c1f70): Downloading (100%)         
  - Installing grandt/relativepath (1.0.2): Downloading (100%)         
  - Installing grandt/binstring (1.0.0): Downloading (100%)         
  - Installing grandt/phpzipmerge (1.0.4): Downloading (100%)         
  - Installing phpzip/phpzip (2.0.8): Downloading (100%)         
  - Installing grandt/phpresizegif (1.0.3): Downloading (100%)         
  - Installing grandt/phpepub (4.0.7.1): Downloading (100%)         
  - Installing hoa/exception (1.17.01.16): Downloading (100%)         
  - Installing hoa/consistency (1.17.05.02): Downloading (100%)         
  - Installing hoa/event (1.17.01.13): Downloading (100%)         
  - Installing hoa/iterator (2.17.01.10): Downloading (100%)         
  - Installing hoa/visitor (2.17.01.16): Downloading (100%)         
  - Installing hoa/ustring (4.17.01.16): Downloading (100%)         
  - Installing hoa/protocol (1.17.01.14): Downloading (100%)         
  - Installing hoa/zformat (1.17.01.10): Downloading (100%)         
  - Installing hoa/regex (1.17.01.13): Downloading (100%)         
  - Installing hoa/math (1.17.05.16): Downloading (100%)         
  - Installing hoa/stream (1.17.02.21): Downloading (100%)         
  - Installing hoa/file (1.17.01.13): Downloading (100%)         
  - Installing hoa/compiler (3.17.01.10): Downloading (100%)         
  - Installing hoa/ruler (2.17.05.16): Downloading (100%)         
  - Installing incenteev/composer-parameter-handler (v2.1.2): Downloading (100%)         
  - Installing wallabag/tcpdf (6.2.15): Downloading (100%)         
  - Installing true/punycode (v2.1.1): Downloading (100%)         
  - Installing smalot/pdfparser (v0.9.26): Downloading (100%)         
  - Installing simplepie/simplepie (1.3.1): Downloading (100%)         
  - Installing monolog/monolog (1.22.1): Downloading (100%)         
  - Installing j0k3r/safecurl (2.0.2): Downloading (100%)         
  - Installing j0k3r/php-readability (1.1.7): Downloading (100%)         
  - Installing j0k3r/graby-site-config (1.0.36): Downloading (100%)         
  - Installing htmlawed/htmlawed (1.1.22): Downloading (100%)         
  - Installing j0k3r/graby (1.8.2): Downloading (100%)         
  - Installing javibravo/simpleue (1.1.1): Downloading (100%)         
  - Installing phpoption/phpoption (1.5.0): Downloading (100%)         
  - Installing phpcollection/phpcollection (0.5.0): Downloading (100%)         
  - Installing jms/parser-lib (1.0.0): Downloading (100%)         
  - Installing jms/metadata (1.6.0): Downloading (100%)         
  - Installing doctrine/instantiator (1.0.5): Downloading (100%)         
  - Installing jms/serializer (1.7.1): Downloading (100%)         
  - Installing kphoen/rulerz (0.20.4): Downloading (100%)         
  - Installing kphoen/rulerz-bundle (0.13.1): Downloading (100%)         
  - Installing doctrine/orm (v2.5.6): Downloading (100%)         
  - Installing lexik/form-filter-bundle (v5.0.4): Downloading (100%)         
  - Installing liip/theme-bundle (1.4.6): Downloading (100%)         
  - Installing mgargano/simplehtmldom (1.5): Downloading (100%)         
  - Installing mnapoli/piwik-twig-extension (1.0.0): Downloading (100%)         
  - Installing michelf/php-markdown (1.7.0): Downloading (100%)         
  - Installing nelmio/api-doc-bundle (2.13.1): Downloading (100%)         
  - Installing nelmio/cors-bundle (1.4.1): Downloading (100%)         
  - Installing php-amqplib/php-amqplib (v2.6.3): Downloading (100%)         
  - Installing php-amqplib/rabbitmq-bundle (v1.12.0): Downloading (100%)         
  - Installing predis/predis (v1.1.1): Downloading (100%)         
  - Installing sonata-project/google-authenticator (1.0.2): Downloading (100%)         
  - Installing scheb/two-factor-bundle (v2.9.0): Downloading (100%)         
  - Installing composer/ca-bundle (1.0.7): Downloading (100%)         
  - Installing sensiolabs/security-checker (v4.0.4): Downloading (100%)         
  - Installing sensio/distribution-bundle (v5.0.20): Downloading (100%)         
  - Installing sensio/framework-extra-bundle (v3.0.26): Downloading (100%)         
  - Installing gedmo/doctrine-extensions (v2.4.28): Downloading (100%)         
  - Installing stof/doctrine-extensions-bundle (v1.2.2): Downloading (100%)         
  - Installing symfony/monolog-bundle (v3.1.0): Downloading (100%)         
  - Installing swiftmailer/swiftmailer (v5.4.8): Downloading (100%)         
  - Installing symfony/swiftmailer-bundle (v2.5.4): Downloading (100%)         
  - Installing twig/extensions (v1.4.1): Downloading (100%)         
  - Installing wallabag/php-mobi (1.0.1): Downloading (100%)         
  - Installing pagerfanta/pagerfanta (v1.0.5): Downloading (100%)         
  - Installing white-october/pagerfanta-bundle (v1.0.8): Downloading (100%)         
  - Installing willdurand/hateoas (2.10.0): Downloading (100%)         
  - Installing jms/serializer-bundle (1.5.0): Downloading (100%)         
  - Installing willdurand/hateoas-bundle (1.2.0): Downloading (100%)         
Generating optimized autoload files
{% endhighlight2 %}

You should then be asked to provide parameters :

~~~vim
> Incenteev\ParameterHandler\ScriptHandler::buildParameters
Creating the "app/config/parameters.yml" file
Some parameters are missing. Please provide them.
database_driver (pdo_sqlite): pdo_mysql
database_host (127.0.0.1):
database_port (null): 3306
database_name (symfony): wallabag
database_user (root): sammy
database_password (null): my_password*84
database_path ('%kernel.root_dir%/../data/db/wallabag.sqlite'): /var/lib/mysql/wallabag
~~~

For the rest of the parameters you can just chose the default values

~~~vim
database_table_prefix (wallabag_):     
database_socket (null):
database_charset (utf8):
mailer_transport (smtp):
mailer_host (127.0.0.1):
mailer_user (null):
mailer_password (null):
locale (en):
secret (ovmpmAWXRCabNlMgzlzFXDYmCFfzGv):
twofactor_auth (true):
twofactor_sender (no-reply@wallabag.org):
fosuser_registration (true):
fosuser_confirmation (true):
from_email (no-reply@wallabag.org):
rss_limit (50):
rabbitmq_host (localhost):
rabbitmq_port (5672):
rabbitmq_user (guest):
rabbitmq_password (guest):
rabbitmq_prefetch_count (10):
redis_scheme (tcp):
redis_host (localhost):
redis_port (6379):
redis_path (null):
redis_password (null):
sites_credentials ({  }):
~~~

The installation then continues

~~~vim
> Sensio\Bundle\DistributionBundle\Composer\ScriptHandler::buildBootstrap
> Sensio\Bundle\DistributionBundle\Composer\ScriptHandler::clearCache

 // Clearing the cache for the prod environment with debug                      
 // false                                                                       


 [OK] Cache for the "prod" environment (debug=false) was successfully cleared.  


> Sensio\Bundle\DistributionBundle\Composer\ScriptHandler::installAssets

 Trying to install assets as relative symbolic links.

 --- ------------------------------ ------------------
      Bundle                         Method / Error    
 --- ------------------------------ ------------------
  ✔   NelmioApiDocBundle             relative symlink  
  ✔   WhiteOctoberPagerfantaBundle   relative symlink  
  ✔   FOSJsRoutingBundle             relative symlink  
 --- ------------------------------ ------------------


 [OK] All assets were successfully installed.                                   
~~~

In step 2, you will be asked if you would like to reset the database, answer `n`
(No).

~~~vim
> Sensio\Bundle\DistributionBundle\Composer\ScriptHandler::installRequirementsFile
Installing wallabag...

Step 1 of 4. Checking system requirements.
+------------------------+--------+----------------+
| Checked                | Status | Recommendation |
+------------------------+--------+----------------+
| PDO Driver (pdo_mysql) | OK!    |                |
| Database connection    | OK!    |                |
| Database version       | OK!    |                |
| curl_exec              | OK!    |                |
| curl_multi_init        | OK!    |                |
+------------------------+--------+----------------+
Success! Your system can run wallabag properly.

Step 2 of 4. Setting up database.
~~~

When asked if you would like to create a new admin user, answer yes and fill
a name and password of your choice:

~~~vim
Step 3 of 4. Administration setup.
Would you like to create a new admin user (recommended) ? (Y/n)y
Username (default: wallabag) :admin          
Password (default: wallabag) :another_password*78
Email:admin@mail.com
~~~

The installation finished:

~~~vim
Step 4 of 4. Config setup.

wallabag has been successfully installed.
Just execute `php bin/console server:run --env=prod` for using wallabag: http://localhost:8000
~~~

You can now copy the wallabag web application where you intended to serve it:

~~~bash
cp -r ~/Git/wallabag/* /var/www/wallabag.sammy.fr/
~~~

And give access to this directory to `www-data`:

~~~bash
sudo chown -R www-data:www-data /var/www/wallabag.sammy.fr
~~~

## Modify Virtual host

If you have already created your virtual host, you will need to modify:
- `wallabag.sammy.fr-le-ssl.conf` : the virtual host configuration for the
HTTPS site
-  `wallabag.sammy.conf` : the virtual host configuration for the HTTP site


~~~bash
vim wallabag.sammy.fr-le-ssl.conf
~~~

Here is the configuration needed

~~~apache
<IfModule mod_ssl.c>
<VirtualHost *:443>
	# The ServerName directive sets the request scheme, hostname and port that
	# the server uses to identify itself. This is used when creating
	# redirection URLs. In the context of virtual hosts, the ServerName
	# specifies what hostname must appear in the request's Host: header to
	# match this virtual host. For the default virtual host (this file) this
	# value is not decisive as it is used as a last resort host regardless.
	# However, you must set it for any further virtual host explicitly.
	#ServerName www.example.com

	ServerAdmin sammy@gmail.com
	ServerName  wallabag.sammy.fr
	ServerAlias www.wallabag.sammy.fr
	DocumentRoot /var/www/wallabag.sammy.fr/web
	<Directory /var/www/wallabag.sammy.fr/web>
        	AllowOverride None
	        Order Allow,Deny
        	Allow from All

	        <IfModule mod_rewrite.c>
        	    Options -MultiViews
	            RewriteEngine On
        	    RewriteCond %{REQUEST_FILENAME} !-f
	            RewriteRule ^(.*)$ app.php [QSA,L]
        	</IfModule>
	</Directory>

	# uncomment the following lines if you install assets as symlinks
	# or run into problems when compiling LESS/Sass/CoffeScript assets
	# <Directory /var/www/wallabag>
	#     Options FollowSymlinks
	# </Directory>

        # optionally disable the RewriteEngine for the asset directories
        # which will allow apache to simply reply with a 404 when files are
   	# not found instead of passing the request into the full symfony stack
        <Directory /var/www/wallabag.sammy.fr/public_html/bundles>
	        <IfModule mod_rewrite.c>
			RewriteEngine Off
        	</IfModule>
	</Directory>
	# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
	# error, crit, alert, emerg.
	# It is also possible to configure the loglevel for particular
	# modules, e.g.
	#LogLevel info ssl:warn

	ErrorLog ${APACHE_LOG_DIR}/wallabag_error.log
	CustomLog ${APACHE_LOG_DIR}/wallabag_access.log combined

	# For most configuration files from conf-available/, which are
	# enabled or disabled at a global level, it is possible to
	# include a line for only one particular virtual host. For example the
	# following line enables the CGI configuration for this host only
	# after it has been globally disabled with "a2disconf".
	#Include conf-available/serve-cgi-bin.conf
SSLCertificateFile /etc/letsencrypt/live/sammy.fr/fullchain.pem
SSLCertificateKeyFile /etc/letsencrypt/live/sammy.fr/privkey.pem
Include /etc/letsencrypt/options-ssl-apache.conf
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
</IfModule>
~~~

{% include info.html content="**Note**: Note that when you copied the contents
of wallabag, you copied a subdirectory called `web`. This is the equivalent of
the usual `public_html` directory : the website document root"%}

Report the modifications to wallabag.sammy.fr.conf and you are good to go!
You just need to restart apache2 to see you web site active:

~~~bash
sudo systemctl restart apache2.service
~~~
