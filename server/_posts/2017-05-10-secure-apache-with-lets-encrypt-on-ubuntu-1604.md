---
date: 2017-05-10
title: "Secure Apache with Let's Encrypt on Ubuntu 16.04"
excerpt: "How to get a free SSL certificate and let your users browse securely"
tags: ['Security', 'Apache', "Let's Encrypt", 'LAMP Stack', 'Ubuntu 16.04', 'Encryption', 'Digital Ocean']

source:
    name: Digital Ocean
    link: https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-16-04
original_author:
    name: Erika Heidi
    link: https://www.digitalocean.com/community/users/erikaheidi
license:
    name: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
    link: https://creativecommons.org/licenses/by-nc-sa/4.0/
author_profile: false
sidebar:
  nav: "self-hosted"
verified: 2017-05-18
---


This tutorial will show you how to set up a TLS/SSL certificate from [Let’s
Encrypt](https://letsencrypt.org/) on an Ubuntu 16.04 server running Apache as
a web server. We will also cover how to automate the certificate renewal
process.

SSL certificates are used within web servers to encrypt the traffic between
the server and client, providing extra security for users accessing your
application. Let’s Encrypt provides an easy way to obtain and install trusted
certificates for free.

## License and legal information

{% include license_author %}

## Prerequisites

In order to complete this guide, you will need:

  * An Ubuntu 16.04 server with a non-root sudo-enabled user, which you can
set up by following our [Initial Server
Setup](https://www.digitalocean.com/community/tutorials/initial-server-setup-
with-ubuntu-16-04) guide

  * The Apache web server installed with [one or more domain
names](https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-
virtual-hosts-on-ubuntu-16-04) properly configured through Virtual Hosts that
specify `ServerName`.

When you are ready to move on, log into your server using your sudo-enabled
account.

## Step 1 — Install the Let's Encrypt Client

Let's Encrypt certificates are fetched via client software running on your
server. The official client is called Certbot, and its developers maintain
their own Ubuntu software repository with up-to-date versions. Because Certbot
is in such active development it's worth using this repository to install a
newer version than Ubuntu provides by default.

First, add the repository:

~~~bash
sudo add-apt-repository ppa:certbot/certbot
~~~

You'll need to press `ENTER` to accept. Afterwards, update the package list to
pick up the new repository's package information:

~~~bash
sudo apt-get update
~~~

And finally, install Certbot from the new repository with `apt-get`:

~~~bash
sudo apt-get install python-certbot-apache
~~~

The `certbot` Let's Encrypt client is now ready to use.

## Step 2 — Set Up the SSL Certificate

Generating the SSL certificate for Apache using Certbot is quite
straightforward. The client will automatically obtain and install a new SSL
certificate that is valid for the domains provided as parameters.

To execute the interactive installation and obtain a certificate that covers
only a single domain, run the `certbot` command like so, where example.com is
your domain:

~~~bash
sudo certbot --apache -d example.com
~~~

If you want to install a single certificate that is valid for multiple domains
or subdomains, you can pass them as additional parameters to the command. The
first domain name in the list of parameters will be the **base** domain used
by Let’s Encrypt to create the certificate, and for that reason we recommend
that you pass the bare top-level domain name as first in the list, followed by
any additional subdomains or aliases:

~~~bash
sudo certbot --apache -d example.com -d www.example.com
~~~

For this example, the **base** domain will be `example.com`.

After the dependencies are installed, you will be presented with a step-by-
step guide to customize your certificate options. You will be asked to provide
an email address for lost key recovery and notices, and you will be able to
choose between enabling both `http` and `https` access or forcing all requests
to redirect to `https`. It is usually safest to require `https`, unless you
have a specific need for unencrypted `http` traffic.

{% include note.html content="Get the command with all your websites with this `find  /etc/apache2/sites-available -type f -name '*.fr.conf'  | rev | cut -d '/' -f1 | cut -d '.' -f2- | rev | xargs -I {} echo \"-d {} -d www.{}\" | tr '\n' ' ' | xargs -I {} echo \"sudo certbot --apache {}\"` "%}

When the installation is finished, you should be able to find the generated
certificate files at `/etc/letsencrypt/live`. You can verify the status of
your SSL certificate with the following link (don’t forget to replace
example.com with your **base** domain):

~~~bash
https://www.ssllabs.com/ssltest/analyze.html?d=example.com&latest
~~~

You should now be able to access your website using a `https` prefix.

## Step 3 — Set Up Auto Renewal

Let's Encrypt's certificates are only valid for ninety days. This is to
encourage users to automate their certificate renewal process. We'll need to
set up a regularly run command to check for expiring certificates and renew
them automatically.

To run the renewal check daily, we will use `cron`, a standard system service
for running periodic jobs. We tell `cron` what to do by opening and editing a
file called a `crontab`.

~~~bash
sudo crontab -e
~~~

Your text editor will open the default crontab which is a text file with some
help text in it. Paste in the following line at the end of the file, then save
and close it:

{% highlight2 vim caption=crontab %}
15 3 * * * /usr/bin/certbot renew --quiet
{% endhighlight2 %}

The `15 3 * * *` part of this line means "run the following command at 3:15
am, every day". You may choose any time.

The `renew` command for Certbot will check all certificates installed on the
system and update any that are set to expire in less than thirty days.
`--quiet` tells Certbot not to output information nor wait for user input.

`cron` will now run this command daily. Because we installed our certificates
using the `--apache` plugin, Apache will also be reloaded to ensure the new
certificates are used.

## Conclusion

In this guide, we saw how to install a free SSL certificate from Let’s Encrypt
in order to secure a website hosted with Apache. We recommend that you check
the official [Let’s Encrypt blog](https://letsencrypt.org/blog/) for important
updates from time to time, and read [the Certbot
documentation](https://certbot.eff.org/docs/) for more details about the
Certbot client.
