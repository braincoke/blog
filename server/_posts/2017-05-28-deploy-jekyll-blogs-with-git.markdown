---
title: "Deploy Jekyll blogs with git"
excerpt: "Learn how to deploy a static website with a git push"
source:
    name: Digital Ocean
    link: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-jekyll-development-site-on-ubuntu-16-04
original_author:
    name: Melissa Anderson
    link: https://www.digitalocean.com/community/users/melissaanderson
license:
    name: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
    link: https://creativecommons.org/licenses/by-nc-sa/4.0/

tags: [Ubuntu 16.04, Jekyll, Git, Deployment]
author_profile: false
sidebar:
  nav: "self-hosted"
verified: 2017-05-28
---
Jekyll is a static-site generator that provides some of the benefits of a Content Management System (CMS) while avoiding the performance and security issues introduced by such database-driven sites. It is "blog-aware" with special features to handle date-organized content, although its usefulness is not limited to blogging sites. Jekyll is well-suited for people who need to work off-line, who prefer to use a lightweight editor instead of web forms for maintaining content, and who wish to use version control to track changes to their website.

In this tutorial, we will install a Jekyll 3.2.1 development site on Ubuntu 16.04. In later tutorials, we'll explore the content generated here, publish a static site to the same server, and eventually deploy to a production location.

## License and legal information

{% include license_author %}

## Prerequisites

To follow this tutorial, you will need:
* An Ubuntu 16.04 server
* with a non-root user with sudo privileges

Once you've completed this prerequisite, you're ready to install Jekyll and its dependencies.

## Step 1 — Installing Jekyll

We’ll start by updating our package list to be sure we have the latest information on the newest versions of packages and their dependencies.

~~~bash
sudo apt-get update
~~~

Then we’ll install Ruby and its development libraries as well as `make` and `gcc` so that Jekyll's libraries will compile once we install Jekyll:

~~~bash
sudo apt-get install ruby ruby-dev make gcc
~~~

When that's complete, we'll use Ruby's `gem` package manager to install Jekyll itself as well as Bundler to manage Gem dependencies:

~~~bash
sudo gem install jekyll bundler
~~~

{% include note.html content="**Note**: installing as root has some drawbacks. There is the possibility to use `gem install jekyll bundler --user-install` but we haven't tested it yet." %}

We repeat these operations on our laptop to be able to develop without an internet connection.

## Step 2 — Create a new development site

On our laptop, from our home directory, we'll use Jekyll's `new` command to create scaffolding for a site in a sub-directory called `docs.sammy.fr`.

~~~bash
cd ~
sudo jekyll new docs.sammy.fr
#Output
New jekyll site installed in /home/sammy/docs.sammy.fr.
~~~

Jekyll specifies its default theme, minima, in its Gemfile. We'll need to run Bundler to install the theme:

~~~bash
cd ~/docs.sammy.fr
bundle install
~~~

To start jekyll web server locally run the command below and access your site through `http://localhost:4000`
~~~bash
jekyll serve
~~~

## Step 3 — Use git to control our versions and deploy

We want to use git to control our versions, record the changes made to the website and deploy it to our web server.


#### Version control
To do so we start by creating a repository on our laptop:

~~~bash
cd ~/docs.sammy.fr
git init .
~~~

We can then make our initial commit:

~~~bash
git add -A
git commit -m "Initial commit"
~~~

#### Automatic deployment
On our __server__ we create the git repository that will hold our project:
~~~bash
mkdir docs.sammy.fr.git
cd docs.sammy.fr.git
git --bare init
~~~

And we create the directory that will hold the compiled website:
~~~bash
mkdir /var/www/docs.sammy.fr
~~~

{% include note.html content="**Note**: You will also need to create a virtual host in the apache configuration file `docs.sammy.fr.conf`, use Let's Encrypt to have a SSL certificate, activate the site with `a2ensite` and restart apache. "%}

We add a post-receive hook so that when the server receives a push from our laptop, the jekyll compilation starts and the compiled files are placed in `/var/www/docs.sammy.fr`.

~~~bash
vim hooks/post-receive
~~~

Here are the contents of the post-receive hook:
~~~vim
GIT_REPO=$HOME/Git/docs.sammy.fr.git
TMP_GIT_CLONE=$HOME/tmp/docs.sammy.fr
PUBLIC_WWW=/var/www/docs.sammy.fr/public_html

rm -rf $TMP_GIT_CLONE
git clone $GIT_REPO $TMP_GIT_CLONE
cd $TMP_GIT_CLONE
bundle exec jekyll build -s $TMP_GIT_CLONE -d $PUBLIC_WWW
rm -rf $TMP_GIT_CLONE
exit
~~~

Now make the hook executable with:
~~~bash
chmod ug+x hooks/post-receive
~~~

If you ever need to log your post-receive hook you can add the following code
at the beginning of the file:
~~~bash
LOG_FILE=/tmp/postreceive.log
# Close STDOUT file descriptor
exec 1<&-
# Close STDERR FD
exec 2<&-
# Open STDOUT as $LOG_FILE file for read and write.
exec 1<>$LOG_FILE
# Redirect STDERR to STDOUT
exec 2>&1
echo "This line will appear in $LOG_FILE, not 'on screen'"
~~~

#### Laptop configuration

We can now run the following command on any laptop that needs to be able to deploy using this hook:

~~~bash
cd ~/docs.sammy.fr
git remote add deploy sammy@sammy.fr:~/Git/docs.sammy.fr.git
~~~

{% include note.html content="**Note**: the user sammy must have a ssh key authorized to push on the server"%}

Deploying is now as easy as running the following on our laptop:

~~~bash
git push deploy master
~~~

## Step 4 — Verification

Run manually the following commands on the server to verify that the compilation succeeds:
~~~bash
GIT_REPO=$HOME/Git/docs.sammy.fr.git
TMP_GIT_CLONE=$HOME/tmp/docs.sammy.fr
PUBLIC_WWW=/var/www/docs.sammy.fr

git clone $GIT_REPO $TMP_GIT_CLONE
jekyll build -s $TMP_GIT_CLONE -d $PUBLIC_WWW
~~~

{:.note .info}
Use `git clone -b <branch_name> <git_repo> <clone_dir>` to clone a specific branch

You might have to install some dependencies with
~~~bash
cd $TMP_GIT_CLONE
bundle install
~~~

You can also run jekyll locally with
~~~bash
bundle exec jekyll serve
~~~

## Step 5 — Authentification

Links:
* [How to set up password authentication with apache on ubuntu]({% post_url 2017-07-11-how-to-set-up-password-authentication-with-apache-on-ubuntu-1404 %})
* [Digital ocean tutorial](https://www.digitalocean.com/community/tutorials/how-to-set-up-password-authentication-with-apache-on-ubuntu-14-04)
