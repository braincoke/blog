---
title: "Install your own IPlookup tool"
excerpt: "Get your public IP without querying an unknown server."
tags: [Ubuntu 16.04, NextCloud, self-hosted]
author_profile: false
sidebar:
  nav: "self-hosted"
verified: 2019-04-04
---

When you want to know your public IP address, you will probably use a tool like

* [ipify.org](https://api.ipify.org)
* [ip-address.me](https://ip-address.me/ip/)
* [ipconfig.co](https://ifconfig.co/)
* [ifconfig.io](https://ifconfig.io/)
* [ifconfig.me](https://ifconfig.me/)
* [myexternalip.com](http://myexternalip.com/raw)

However if we don't want to rely entirely on an external service and we don't feel like 
giving our IP address to unknown strangers we might want to self-host.

To do this we use [echoip](https://github.com/mpolden/echoip), a self-hosted IP lookup 
solution written in `Go`.

## Docker-compose

We use the docker container provided by echoip's maintainer to deploy the tool.
Here is our docker-compose file :

~~~yml
version: '2'
services:
  echoip:
    container_name: 'echoip'
    image: 'mpolden/echoip:latest'
    restart: unless-stopped
    ports:
      - '8085:8080'
~~~

Now we just have to start it with `docker-compose up -d`. 
Of course, we need `Docker` and `docker-compose` installed for it to work.

## Nginx config

Now we want to serve our service through `ifconfig.domain.com`.
To do so we configure our subdomain with our domain provider and configure Nginx with a reverse proxy.

Here is the Nginx configuration in the file `/etc/nginx/sites-available/ifconfig.domain.com`:
~~~nginx
server {
    listen *:80;
    server_name eifconfig.domain.com www.ifconfig.domain.com;

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_pass http://127.0.0.1:8085;
    }
}
~~~

We then deploy a certificate for this subdomain with Let's Encrypt
~~~nginx
sudo certbot --nginx -d ifconfig.domain.com -d www.ifconfig.domain.com
~~~

And finally we can activate the site :
~~~bash
sudo ln -s /etc/nginx/sites-available/ifconfig.domain.com /etc/nginx/sites-enabled
sudo nginx reload
~~~
